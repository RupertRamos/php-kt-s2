<?php require_once "./code.php"; 

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S2: Repition Control Structures and Array Manipulation</title>
</head>
<body>

	<h1>Repition Control Structures</h1>

	<?php whileLoop(); ?>

	<?php doWhileLoop(); ?>

	<?php forLoop(); ?>

	<?php modifiedForLoop(); ?>

	
	<h2>Associative Array</h2>
	
	<?php echo $gradePeriods['secondGrading']; ?>
	<?php echo $gradePeriods -> firstGrading; ?>

	<ul>
		<?php foreach($gradePeriods as $period => $grade) { ?>
			<li>Grade in <?= $period ?> is <?= $grade ?></li>
		<?php } ?>
		
	</ul>

	<h2>Two-Dimensional Array</h2>
	<?php echo $heroes[2][1]; ?>


	<ul>
		<?php 
			foreach($heroes as $team) {
				foreach($team as $member) {
					?>

					<li><?php echo $member ?></li>

					<?php
				}
			}
		?>
	</ul>

	<h2>Sorting</h2>

	<pre>
		<?php print_r($sortedBrands); ?>
	</pre>

	<h2>Reverse Sorting</h2>

	<pre>
		<?php print_r($reverseSortedBrands); ?>
	</pre>

	<h2>Array Mutations (Append)</h2>


	<h3>Push Method</h3>
	<?php array_push($computerBrands, 'Apple') ?>

	<pre>
		<?php print_r($computerBrands); ?>
	</pre>

	<h3>Unshift Method</h3>

	<?php array_unshift($computerBrands, 'Dell')?>

	<pre>
		<?php print_r($computerBrands); ?>
	</pre>

	<h3>Pop Method</h3>

		<?php array_pop($computerBrands)?>

	<pre>
		<?php print_r($computerBrands); ?>
	</pre>

	<h3>Shift Method</h3>

	<?php array_shift($computerBrands)?>

	<pre>
		<?php print_r($computerBrands); ?>
	</pre>

	<h3>Count</h3>

	<pre>
		<?php echo count($computerBrands); ?>
	</pre>

	<p>
		<?php echo searchBrand($computerBrands, 'Asus'); ?>
		<?php echo searchBrand($computerBrands, 'acer'); ?>
	</p>

	<pre>
		<?php print_r($reverseSortedBrands); ?>
	</pre>


	<h2>S2 Activity</h2>

	<h4>For Loop</h4>

	<?php divLoop(); ?>

	<h4>Array Manipulation</h4>

	<?php array_push($students, 'John Smith') ?>
	<pre>
		<?php print_r($students); ?>
	</pre>

	<?php array_push($students, 'Jane Smith') ?>
	<pre>
		<?php print_r($students); ?>
	</pre>

	<h4>Count</h4>
	<pre>
		<?php echo count($students); ?>
	</pre>

	<?php array_shift($students)?>

	<pre>
		<?php print_r($students); ?>
	</pre>

</body>
</html>